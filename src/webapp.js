"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var path = require("path");
var Pool = require("pg").Pool;
var _a = require("express-openid-connect"), auth = _a.auth, requiresAuth = _a.requiresAuth;
var bodyParser = require("body-parser");
var fs = require("fs");
var https = require("https");
var bcrypt = require("bcrypt");
var dotenv = require("dotenv");
dotenv.config();
var app = express();
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
var pool = new Pool({
    host: process.env.HOSTNAME,
    port: 5432,
    user: process.env.USER,
    password: process.env.PASSWORD,
    database: "projekt_ralm",
    ssl: true,
});
var externalUrl = process.env.RENDER_EXTERNAL_URL;
var port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;
var config = {
    authRequired: false,
    idpLogout: true,
    secret: process.env.SECRET,
    baseURL: externalUrl || "https://localhost:".concat(port),
    clientID: process.env.CLIENT_ID,
    issuerBaseURL: "https://dev-rqsk086kqek22pm4.us.auth0.com",
    clientSecret: process.env.CLIENT_SECRET,
    authorizationParams: {
        response_type: "code",
    },
};
app.use(auth(config));
app.use(bodyParser.urlencoded({ extended: false }));
app.get("/", function (req, res) {
    res.render("index");
});
app.post("/prijava", function (req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var odgovor, korisnickoIme, lozinka, filterZnakovi, podaci, podaciZaPrikaz, podaci, podaciZaPrikaz, poruka, poruka, podaci, podaciZaPrikaz, podaci, podaciZaPrikaz, poruka, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    odgovor = req.body.odgovor;
                    korisnickoIme = req.body.korisnickoIme;
                    lozinka = req.body.lozinka;
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 13, , 14]);
                    if (!(odgovor == "Da")) return [3 /*break*/, 8];
                    filterZnakovi = /["';-]|(OR)|(or)|(Or)|(oR)/;
                    if (!!filterZnakovi.test(korisnickoIme)) return [3 /*break*/, 6];
                    if (!(korisnickoIme == "admin" && lozinka == "adminpass123")) return [3 /*break*/, 3];
                    return [4 /*yield*/, pool.query("SELECT * FROM \"korisnici_demo\"")];
                case 2:
                    podaci = _a.sent();
                    podaciZaPrikaz = podaci.rows;
                    res.render("podaci", { podaci: podaciZaPrikaz });
                    return [3 /*break*/, 5];
                case 3: return [4 /*yield*/, pool.query("SELECT * FROM \"korisnici_demo\" WHERE \"korisnickoIme\" = $1 AND \"lozinka\" = $2", [korisnickoIme, lozinka])];
                case 4:
                    podaci = _a.sent();
                    podaciZaPrikaz = podaci.rows;
                    if (podaciZaPrikaz.length == 0) {
                        poruka = "Pokušajte se prijaviti ponovno.";
                        res.render("index", { porukaInj: poruka });
                    }
                    else {
                        res.render("podaci", { podaci: podaciZaPrikaz });
                    }
                    _a.label = 5;
                case 5: return [3 /*break*/, 7];
                case 6:
                    poruka = "Detektiran je SQL injection napad!";
                    res.render("index", { porukaInj: poruka });
                    _a.label = 7;
                case 7: return [3 /*break*/, 12];
                case 8:
                    if (!(korisnickoIme == "admin" && lozinka == "adminpass123")) return [3 /*break*/, 10];
                    return [4 /*yield*/, pool.query("SELECT * FROM \"korisnici_demo\"")];
                case 9:
                    podaci = _a.sent();
                    podaciZaPrikaz = podaci.rows;
                    res.render("podaci", { podaci: podaciZaPrikaz });
                    return [3 /*break*/, 12];
                case 10: return [4 /*yield*/, pool.query("SELECT * FROM \"korisnici_demo\" WHERE \"korisnickoIme\" = '".concat(korisnickoIme, "' AND \"lozinka\" = '").concat(lozinka, "'"))];
                case 11:
                    podaci = _a.sent();
                    podaciZaPrikaz = podaci.rows;
                    if (podaciZaPrikaz.length == 0) {
                        poruka = "Pokušajte se prijaviti ponovno.";
                        res.render("index", { porukaInj: poruka });
                    }
                    else {
                        res.render("podaci", { podaci: podaciZaPrikaz });
                    }
                    _a.label = 12;
                case 12: return [3 /*break*/, 14];
                case 13:
                    error_1 = _a.sent();
                    res.status(500).send("Došlo je do pogreške!");
                    return [3 /*break*/, 14];
                case 14: return [2 /*return*/];
            }
        });
    });
});
app.post("/autentifikacija", function (req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var odgovor, korisnickoIme, lozinka, podaci, podaciRedovi, redakSPodacima, _i, podaciRedovi_1, redak, podudaranje, poruka, podaci, podaciRedovi, redakSPodacima, redakSKorisnickimImenom, redakSLozinkom, poruka, poruka, poruka, error_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    odgovor = req.body.autentifikacijaOdgovor;
                    korisnickoIme = req.body.autentifikacijaKorisnickoIme;
                    lozinka = req.body.autentifikacijaLozinka;
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 10, , 11]);
                    if (!(odgovor == "Da")) return [3 /*break*/, 7];
                    return [4 /*yield*/, pool.query("SELECT * FROM \"korisnici_hash\"")];
                case 2:
                    podaci = _a.sent();
                    podaciRedovi = podaci.rows;
                    redakSPodacima = null;
                    _i = 0, podaciRedovi_1 = podaciRedovi;
                    _a.label = 3;
                case 3:
                    if (!(_i < podaciRedovi_1.length)) return [3 /*break*/, 6];
                    redak = podaciRedovi_1[_i];
                    if (!(redak.korisnickoIme == korisnickoIme)) return [3 /*break*/, 5];
                    return [4 /*yield*/, bcrypt.compare(lozinka, redak.lozinka)];
                case 4:
                    podudaranje = _a.sent();
                    if (podudaranje) {
                        redakSPodacima = redak;
                    }
                    _a.label = 5;
                case 5:
                    _i++;
                    return [3 /*break*/, 3];
                case 6:
                    if (redakSPodacima) {
                        if (korisnickoIme == "admin") {
                            res.render("autentifikacija", { podaci: podaciRedovi });
                        }
                        else {
                            res.render("autentifikacija");
                        }
                    }
                    else {
                        poruka = "Pokušajte se prijaviti ponovno.";
                        res.render("index", { poruka: poruka });
                    }
                    return [3 /*break*/, 9];
                case 7: return [4 /*yield*/, pool.query("SELECT * FROM \"korisnici_demo\"")];
                case 8:
                    podaci = _a.sent();
                    podaciRedovi = podaci.rows;
                    redakSPodacima = podaciRedovi.find(function (redak) {
                        return redak.korisnickoIme == korisnickoIme && redak.lozinka == lozinka;
                    });
                    redakSKorisnickimImenom = podaciRedovi.find(function (redak) { return redak.korisnickoIme == korisnickoIme; });
                    redakSLozinkom = podaciRedovi.find(function (redak) { return redak.lozinka == lozinka; });
                    if (redakSPodacima) {
                        if (korisnickoIme == "admin" && lozinka == "adminpass123") {
                            res.render("autentifikacija", { podaci: podaciRedovi });
                        }
                        else {
                            res.render("autentifikacija");
                        }
                    }
                    else if (redakSKorisnickimImenom) {
                        if (redakSKorisnickimImenom.lozinka != lozinka) {
                            poruka = "Korisničko ime postoji u bazi podataka, ali lozinka je netočna.";
                            res.render("index", { poruka: poruka });
                        }
                    }
                    else if (redakSLozinkom) {
                        if (redakSLozinkom.korisnickoIme != korisnickoIme) {
                            poruka = "Lozinka postoji u bazi podataka, ali uneseno je pogrešno korisničko ime.";
                            res.render("index", { poruka: poruka });
                        }
                    }
                    else {
                        poruka = "Korisnik ne postoji u bazi podataka.";
                        res.render("index", { poruka: poruka });
                    }
                    _a.label = 9;
                case 9: return [3 /*break*/, 11];
                case 10:
                    error_2 = _a.sent();
                    res.status(500).send("Došlo je do pogreške!");
                    return [3 /*break*/, 11];
                case 11: return [2 /*return*/];
            }
        });
    });
});
if (externalUrl) {
    var hostname_1 = "0.0.0.0";
    app.listen(port, hostname_1, function () {
        console.log("Server locally running at http://".concat(hostname_1, ":").concat(port, "/ and from outside on ").concat(externalUrl));
    });
}
else {
    https
        .createServer({
        key: fs.readFileSync("server.key"),
        cert: fs.readFileSync("server.cert"),
    }, app)
        .listen(port, function () {
        console.log("Server running at https://localhost:".concat(port, "/"));
    });
}
