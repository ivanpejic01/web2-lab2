import { type } from "os";

const express = require("express");
const path = require("path");
const { Pool } = require("pg");
const { auth, requiresAuth } = require("express-openid-connect");
const bodyParser = require("body-parser");
const fs = require("fs");
const https = require("https");
const bcrypt = require("bcrypt");

const dotenv = require("dotenv");
dotenv.config();

const app = express();
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

const pool = new Pool({
  host: process.env.HOSTNAME,
  port: 5432,
  user: process.env.USER,
  password: process.env.PASSWORD,
  database: "projekt_ralm",
  ssl: true,
});

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port =
  externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;

const config = {
  authRequired: false,
  idpLogout: true,
  secret: process.env.SECRET,
  baseURL: externalUrl || `https://localhost:${port}`,
  clientID: process.env.CLIENT_ID,
  issuerBaseURL: "https://dev-rqsk086kqek22pm4.us.auth0.com",
  clientSecret: process.env.CLIENT_SECRET,
  authorizationParams: {
    response_type: "code",
  },
};

app.use(auth(config));
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", function (req, res) {
  res.render("index");
});

app.post("/prijava", async function (req, res) {
  const odgovor = req.body.odgovor;
  const korisnickoIme = req.body.korisnickoIme;
  const lozinka = req.body.lozinka;

  try {
    if (odgovor == "Da") {
      const filterZnakovi = /["';-]|(OR)|(or)|(Or)|(oR)/;
      if (!filterZnakovi.test(korisnickoIme)) {
        if (korisnickoIme == "admin" && lozinka == "adminpass123") {
          const podaci = await pool.query(`SELECT * FROM "korisnici_demo"`);
          const podaciZaPrikaz = podaci.rows;
          res.render("podaci", { podaci: podaciZaPrikaz });
        } else {
          const podaci = await pool.query(
            `SELECT * FROM "korisnici_demo" WHERE "korisnickoIme" = $1 AND "lozinka" = $2`,
            [korisnickoIme, lozinka]
          );
          const podaciZaPrikaz = podaci.rows;
          if (podaciZaPrikaz.length == 0) {
            const poruka = "Pokušajte se prijaviti ponovno.";
            res.render("index", { porukaInj: poruka });
          } else {
            res.render("podaci", { podaci: podaciZaPrikaz });
          }
        }
      } else {
        const poruka = "Detektiran je SQL injection napad!";
        res.render("index", { porukaInj: poruka });
      }
    } else {
      if (korisnickoIme == "admin" && lozinka == "adminpass123") {
        const podaci = await pool.query(`SELECT * FROM "korisnici_demo"`);
        const podaciZaPrikaz = podaci.rows;
        res.render("podaci", { podaci: podaciZaPrikaz });
      } else {
        const podaci = await pool.query(
          `SELECT * FROM "korisnici_demo" WHERE "korisnickoIme" = '${korisnickoIme}' AND "lozinka" = '${lozinka}'`
        );
        const podaciZaPrikaz = podaci.rows;
        if (podaciZaPrikaz.length == 0) {
          const poruka = "Pokušajte se prijaviti ponovno.";
          res.render("index", { porukaInj: poruka });
        } else {
          res.render("podaci", { podaci: podaciZaPrikaz });
        }
      }
    }
  } catch (error) {
    res.status(500).send("Došlo je do pogreške!");
  }
});

app.post("/autentifikacija", async function (req, res) {
  const odgovor = req.body.autentifikacijaOdgovor;
  const korisnickoIme = req.body.autentifikacijaKorisnickoIme;
  const lozinka = req.body.autentifikacijaLozinka;
  try {
    if (odgovor == "Da") {
      const podaci = await pool.query(`SELECT * FROM "korisnici_hash"`);
      const podaciRedovi = podaci.rows;

      let redakSPodacima = null;
      for (const redak of podaciRedovi) {
        if (redak.korisnickoIme == korisnickoIme) {
          const podudaranje = await bcrypt.compare(lozinka, redak.lozinka);
          if (podudaranje) {
            redakSPodacima = redak;
          }
        }
      }

      if (redakSPodacima) {
        if (korisnickoIme == "admin") {
          res.render("autentifikacija", { podaci: podaciRedovi });
        } else {
          res.render("autentifikacija");
        }
      } else {
        const poruka = "Pokušajte se prijaviti ponovno.";
        res.render("index", { poruka });
      }
    } else {
      const podaci = await pool.query(`SELECT * FROM "korisnici_demo"`);
      const podaciRedovi = podaci.rows;
      const redakSPodacima = podaciRedovi.find((redak) => {
        return redak.korisnickoIme == korisnickoIme && redak.lozinka == lozinka;
      });
      const redakSKorisnickimImenom = podaciRedovi.find(
        (redak) => redak.korisnickoIme == korisnickoIme
      );
      const redakSLozinkom = podaciRedovi.find(
        (redak) => redak.lozinka == lozinka
      );

      if (redakSPodacima) {
        if (korisnickoIme == "admin" && lozinka == "adminpass123") {
          res.render("autentifikacija", { podaci: podaciRedovi });
        } else {
          res.render("autentifikacija");
        }
      } else if (redakSKorisnickimImenom) {
        if (redakSKorisnickimImenom.lozinka != lozinka) {
          const poruka =
            "Korisničko ime postoji u bazi podataka, ali lozinka je netočna.";
          res.render("index", { poruka });
        }
      } else if (redakSLozinkom) {
        if (redakSLozinkom.korisnickoIme != korisnickoIme) {
          const poruka =
            "Lozinka postoji u bazi podataka, ali uneseno je pogrešno korisničko ime.";
          res.render("index", { poruka });
        }
      } else {
        const poruka = "Korisnik ne postoji u bazi podataka.";
        res.render("index", { poruka });
      }
    }
  } catch (error) {
    res.status(500).send("Došlo je do pogreške!");
  }
});

if (externalUrl) {
  const hostname = "0.0.0.0";
  app.listen(port, hostname, () => {
    console.log(
      `Server locally running at http://${hostname}:${port}/ and from outside on ${externalUrl}`
    );
  });
} else {
  https
    .createServer(
      {
        key: fs.readFileSync("server.key"),
        cert: fs.readFileSync("server.cert"),
      },
      app
    )
    .listen(port, function () {
      console.log(`Server running at https://localhost:${port}/`);
    });
}
